terraform {
  required_providers {
    kustomization = {
      source  = "kbst/kustomization"
      version = ">= 0.2"
    }
  }
  required_version = ">= 0.12"
}

provider "kustomization" {}

data "kustomization_build" "harbor" {
  path = "${abspath(path.module)}/manifests"
}

resource "kustomization_resource" "harbor" {
  for_each = data.kustomization_build.harbor.ids

  manifest = data.kustomization_build.harbor.manifests[each.value]
}
